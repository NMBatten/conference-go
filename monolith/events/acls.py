from .keys import PEXEL_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_photo(city, state):
    url = "https://api.pexels.com/v1/search"
    header = {"Authorization": PEXEL_API_KEY}
    params = {"query": f"{city} {state}"}
    response = requests.get(url, headers=header, params=params)
    content = response.json()
    try:
        return content["photos"][0]["src"]["original"]
    except [KeyError, IndexError]:
        return None

def get_weather(city, state):
    lat_lon_url = f"https://api.openweathermap.org/geo/1.0/direct?q={city},US-{state},USA&appid={OPEN_WEATHER_API_KEY}"
    lat_lon_response = requests.get(lat_lon_url)

    loc_content = lat_lon_response.json()
    lat = loc_content[0]["lat"]
    lon = loc_content[0]["lon"]

    weather_url = f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}"
    response = requests.get(weather_url, params={"units": "imperial"})
    content = response.json()
    try:
        weather_description = content["weather"][0]["description"]
        temperature = content["main"]["temp"]
        return {
            "description": weather_description,
            "temp": temperature,
        }
    except [KeyError, IndexError]:
        return None
